package selenium.layers;

import org.openqa.selenium.WebDriver;
import selenium.pages.MatchesPage;
import selenium.pages.SignInPage;
import selenium.utilities.Score;

import java.util.Dictionary;


public class BusinessLogicLayer {

    private WebDriver driver;

    public BusinessLogicLayer(WebDriver driver) {
        this.driver = driver;
    }

    public String getMainNewsTitle() {
        return new SignInPage(driver).signIn()
                .newsBtnClick()
                .mainNewsTitle();
    }

    public String[] getSecondaryNewsTitles() {
        return new SignInPage(driver).signIn()
                .newsBtnClick()
                .getSecondaryNewsTitles().toArray(new String[0]);
    }

    public String getTitleOfMainNewsByRegion() {
        return new SignInPage(driver).signIn()
                .newsBtnClick()
                .openMainTitleRegionNews()
                .getTitleOfLocationArticle();
    }

    public String getUrlAfterSubmittingFormWithBadCredentials(Dictionary<String, String> value) {
        new SignInPage(driver).signIn()
                .newsBtnClick()
                .coronaVirusTabClick()
                .tellUsYourStoryOpen().fillForm(value);
        return driver.getCurrentUrl();
    }

    public Score matchScoreOfTheChosenMatch(String championship, String month, String teamOne, String teamTwo) {
        return new SignInPage(driver).signIn()
                .sportsBtnClick()
                .footballBtnClick()
                .sendParamsToSearchField(championship)
                .monthChoose(month)
                .getScore(teamOne, teamTwo);
    }

    public Score getMatchScoreFromMatchPage(String teamOne, String teamTwo) {
        return new MatchesPage(driver)
                .openMatch(teamOne, teamTwo)
                .getScoreFromMatchPage();
    }

}
