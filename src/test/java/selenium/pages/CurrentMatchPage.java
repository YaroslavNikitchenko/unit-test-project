package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.utilities.Score;

public class CurrentMatchPage extends BasePage {
    @FindBy(xpath = "//div[contains(@class, 'header')]//span[contains(@class, 'fixture__number--home')]")
    private WebElement homeTeamScoredGoals;

    @FindBy(xpath = "//div[contains(@class, 'header')]//span[contains(@class, 'fixture__number--away')]")
    private WebElement awayTeamScoredGoals;

    public CurrentMatchPage(WebDriver driver) {
        super(driver);
    }

    public Score getScoreFromMatchPage() {
        return new Score(Integer.parseInt(homeTeamScoredGoals.getText()), Integer.parseInt(awayTeamScoredGoals.getText()));
    }
}
