package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronaVirusPage extends BasePage {
    @FindBy(xpath = "//nav[@class = 'nw-c-nav__wide-secondary']//span[text()='Your Coronavirus Stories']")
    private WebElement yourCoronavirusStory;

    @FindBy(xpath = "//a[@href='/news/10725415']")
    private WebElement shareWithBBC;

    public CoronaVirusPage(WebDriver driver) {
        super(driver);
    }

    public StoryPage tellUsYourStoryOpen() {
        yourCoronavirusStory.click();
        navLinksWait();
        shareWithBBC.click();
        navLinksWait();
        return new StoryPage(driver);
    }
}
