package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(id = "idcta-link")
    private WebElement signinPageBtn;

    @FindBy(id = "user-identifier-input")
    private WebElement login;

    @FindBy(id = "password-input")
    private WebElement password;

    @FindBy(id = "submit-button")
    private WebElement signinBtn;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public HomePage signIn() {
        signinPageBtn.click();
        signInFieldsWait();
        final String email = "nickitchenkoyarik@gmail.com";
        login.sendKeys(email);
        final String pass = "test12345";
        password.sendKeys(pass);
        signinBtn.click();
        navLinksWait();
        return new HomePage(driver);
    }
}
