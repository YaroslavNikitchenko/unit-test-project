package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainTitleRegionNews extends BasePage {
    @FindBy(xpath = "//div[@aria-labelledby='featured-contents']//h3[contains(@class, 'paragon-bold')]")
    private WebElement locationMainArticle;
    public MainTitleRegionNews(WebDriver driver) {
        super(driver);
    }
    public String getTitleOfLocationArticle() {
        return locationMainArticle.getText();
    }
}
