package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.utilities.Score;

import java.util.List;

public class MatchesPage extends BasePage {
    @FindBy(id = "downshift-0-input")
    private WebElement championshipSearchField;

    public MatchesPage(WebDriver driver) {
        super(driver);
    }

    public MatchesPage sendParamsToSearchField(String value) {
        championshipSearchField.sendKeys(value);
        championshipSearchField.sendKeys(Keys.DOWN);
        championshipSearchField.sendKeys(Keys.RETURN);
        sliderWait();
        return new MatchesPage(driver);
    }

    public MatchesPage monthChoose(String testMonth) {
        List<WebElement> months = driver.findElements(By.xpath("//a[@class='sp-c-date-picker-timeline__item-inner']"));
        String month;
        for (WebElement webElement : months) {
            month = webElement.getText();
            if (month.contains(testMonth)) {
                webElement.click();
                break;
            }
        }
        matchesListWait();
        return new MatchesPage(driver);
    }

    public CurrentMatchPage openMatch(String teamOne, String teamTwo) {
        List<WebElement> teams = driver.findElements(By.xpath("//article[@class = 'sp-c-fixture']"));
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getText().contains(teamOne) && teams.get(i).getText().contains(teamTwo)) {
                teams.get(i).click();
                break;
            }
        }
        return new CurrentMatchPage(driver);
    }

    public Score getScore(String teamOne, String teamTwo) {
        Score res = null;
        List<WebElement> teams = driver.findElements(By.xpath("//article[@class = 'sp-c-fixture']"));
        List<WebElement> teamOneGoals = driver.findElements(By.xpath("//span[contains(@class, 'fixture__number--home')]"));
        List<WebElement> teamTwoGoals = driver.findElements(By.xpath("//span[contains(@class, 'fixture__number--away')]"));
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getText().contains(teamOne) && teams.get(i).getText().contains(teamTwo)) {
                res = new Score(Integer.parseInt(teamOneGoals.get(i).getText()), Integer.parseInt(teamTwoGoals.get(i).getText()));
                break;
            }
        }
        return res;
    }
}
