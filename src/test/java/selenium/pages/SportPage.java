package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SportPage extends BasePage {
    @FindBy(xpath = "//div[@role='menubar']//a[@data-stat-title='Football']")
    private WebElement footballBtn;

    @FindBy(xpath = "//ul[@role='menu']//a[@data-stat-title='Scores & Fixtures']")
    private WebElement scoresFixturesBtn;


    public SportPage(WebDriver driver) {
        super(driver);
    }


    public MatchesPage footballBtnClick() {
        footballBtn.click();
        navLinksWait();
        scoresFixturesBtn.click();
        sliderWait();
        return new MatchesPage(driver);
    }
}
