package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class NewsPage extends BasePage {


    @FindBy(xpath = "//div[contains(@class, 'primary-item')]//div[contains(@class, 'block@m')]//h3")
    private WebElement mainNews;

    @FindBy(xpath = "//div[contains(@class, 'primary-item')]//div[contains(@class, 'block@m')]//li/a")
    private WebElement mainNewsLocationLink;

    @FindBy(id = "site-container")
    private WebElement allNews;

    @FindBy(id = "orb-search-q")
    private WebElement searchField;

    @FindBy(xpath = "//nav[@class='nw-c-nav__wide']//span[text()='Coronavirus']")
    private WebElement coronavirusTab;

    @FindBy(id = "orb-search-button")
    private WebElement searchBtn;


    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public String mainNewsTitle() {
        return mainNews.getText();
    }

    public CoronaVirusPage coronaVirusTabClick() {
        coronavirusTab.click();
        navLinksWait();
        return new CoronaVirusPage(driver);
    }

    public ArrayList<String> getSecondaryNewsTitles() {
        ArrayList<String> res = new ArrayList<>();
        List<WebElement> titles = allNews.findElements(By.xpath("//div[contains(@class, 'secondary-item')]//h3"));
        for (WebElement webElement : titles) {
            String title = webElement.getText();
            res.add(title);
        }
        return res;
    }

    public MainTitleRegionNews openMainTitleRegionNews() {
        driver.get(mainNewsLocationLink.getAttribute("href"));
        regionMainNewsWait();
        return new MainTitleRegionNews(driver);
    }


}
