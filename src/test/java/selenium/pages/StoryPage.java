package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Dictionary;

public class StoryPage extends BasePage {
    @FindBy(css = "textarea")
    private WebElement story;

    @FindBy(xpath = "//button[text()='Submit']")
    private WebElement submitBtn;

    @FindBy(xpath = "//input[@placeholder='Name']")
    private WebElement name;

    @FindBy(xpath = "//input[@placeholder='Email address']")
    private WebElement email;

    @FindBy(xpath = "//input[@placeholder=\"Contact number \"]")
    private WebElement phonenumber;

    @FindBy(xpath = "//input[@placeholder=\"Location \"]")
    private WebElement location;

    @FindBy(xpath = "//div[@class = 'checkbox']//p[contains(text(),'my name')]")
    private WebElement namePublish;

    @FindBy(xpath = "//div[@class = 'checkbox']//p[contains(text(),'16')]")
    private WebElement years;

    @FindBy(xpath = "//div[@class = 'checkbox']//p[contains(text(),'I accept')]")
    private WebElement termsOfServices;

    public StoryPage(WebDriver driver) {
        super(driver);
    }

    public StoryPage fillForm(Dictionary<String, String> dict) {
        formWait();
        story.sendKeys(dict.get("story"));
        name.sendKeys(dict.get("name"));
        email.sendKeys(dict.get("email"));
        phonenumber.sendKeys(dict.get("phoneNumber"));
        location.sendKeys(dict.get("location"));
        if (dict.get("years").equals("true")) {
            years.click();
        }
        if (dict.get("termsOfService").equals("true")) {
            termsOfServices.click();
        }
        if (dict.get("namePublish").equals("true")) {
            namePublish.click();
        }
        submitBtn.click();
        return new StoryPage(driver);
    }
}
