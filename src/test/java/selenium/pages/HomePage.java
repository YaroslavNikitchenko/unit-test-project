package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    @FindBy(xpath = "//nav[@role = 'navigation']//li[@class = 'orb-nav-newsdotcom']/a[contains(text(), 'News')]")
    private WebElement newsBtn;

    @FindBy(xpath = "//nav[@role = 'navigation']//li[@class = 'orb-nav-sport']/a[text() ='Sport']")
    private WebElement sportsBtn;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public NewsPage newsBtnClick() {
        newsBtn.click();
        navLinksWait();
        return new NewsPage(driver);
    }

    public SportPage sportsBtnClick() {
        sportsBtn.click();
        navLinksWait();
        return new SportPage(driver);
    }
}
