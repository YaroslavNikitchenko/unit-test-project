package selenium.utilities;

import java.util.Dictionary;
import java.util.Hashtable;

public class DictionaryCreator {
    public static Dictionary<String, String> dictionaryCreate(String story, String name,
                                                              String email, String phoneNumber,
                                                              String location, String years,
                                                              String termsOfService, String namePublish) {
        Dictionary<String, String> dictionary = new Hashtable<>();
        dictionary.put("story", story);
        dictionary.put("name", name);
        dictionary.put("email", email);
        dictionary.put("phoneNumber", phoneNumber);
        dictionary.put("location", location);
        dictionary.put("years", years);
        dictionary.put("termsOfService", termsOfService);
        dictionary.put("namePublish", namePublish);
        return dictionary;
    }
}
