package selenium.utilities;

public class Score {
    private final int teamOneGoals;
    private final int teamTwoGoals;

    public Score(int goalsOne, int goalsTwo) {
        teamOneGoals = goalsOne;
        teamTwoGoals = goalsTwo;
    }

    public final String matchScore() {
        return teamOneGoals + "-" + teamTwoGoals;
    }


}
