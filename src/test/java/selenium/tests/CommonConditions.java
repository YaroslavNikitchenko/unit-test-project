package selenium.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import selenium.layers.BusinessLogicLayer;

public class CommonConditions {
    private final String BBC_URL = "https://www.bbc.com";
    protected WebDriver driver;
    BusinessLogicLayer businessLogicLayer;

    @BeforeTest
    public void profileSetUp() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        businessLogicLayer = new BusinessLogicLayer(driver);
        driver.manage().window().maximize();
        driver.get(BBC_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
