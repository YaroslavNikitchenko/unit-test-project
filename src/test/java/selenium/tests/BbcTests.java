package selenium.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import selenium.layers.BusinessLogicLayer;
import selenium.utilities.Score;

import java.util.Dictionary;

import static selenium.utilities.DictionaryCreator.dictionaryCreate;

public class BbcTests extends CommonConditions {
    private static final Dictionary<String, String> DICTIONARY = dictionaryCreate("story", "name",
            "email@@gmail,com",
            "12345", "Ukraine", "true", "false", "true");
    private static final String TITLE = "Thousands hold 'anti-corona' protests in Berlin";
    private static final String[] SECONDARY_TITLES = {
            "'A true superhero' - Chadwick Boseman remembered",
            "How Black Panther inspired children - and adults", "'Change is slow in America'",
            "Elon Musk unveils pig with chip in its brain",
            "Teen billed for police overtime after BLM rally"
    };
    private static final Score SCORE = new Score(2, 0);

    @Test
    public void testMainNewsTitle() {
        String mainTitle = businessLogicLayer.getMainNewsTitle();
        Assert.assertEquals(TITLE, mainTitle, "Tittle of given main news differ from actual");
    }

    @Test
    public void testSecondaryTitles() {
        String[] secondaryTitles = businessLogicLayer.getSecondaryNewsTitles();
        Assert.assertEquals(SECONDARY_TITLES, secondaryTitles, "Tittle of given secondary news differ " +
                "from actual");
    }

    @Test
    public void testRegionMainTitle() {
        String regionTitle = businessLogicLayer.getTitleOfMainNewsByRegion();
        Assert.assertEquals(TITLE, regionTitle, "Tittle of main news by region differ from actual");
    }

    @Test
    public void testFillFormCoronaVirus() {
        String currentUrl = businessLogicLayer.getUrlAfterSubmittingFormWithBadCredentials(DICTIONARY);
        final String storyPageUrl = "https://www.bbc.com/news/10725415";
        Assert.assertEquals(storyPageUrl, currentUrl, "There is no redirect to the current page after " +
                "submitting form with bad credentials");
    }

    @Test
    public void testScoresOfTeams() {
        final String month = "OCT\n2019";
        final String championship = "Scottish Championship";
        final String teamOne = "Dunfermline";
        final String teamTwo = "Arbroath";
        Score matchScoreOfTheChosenMatch = new BusinessLogicLayer(driver).matchScoreOfTheChosenMatch(championship,
                month, teamOne, teamTwo);
        Assert.assertEquals(matchScoreOfTheChosenMatch.matchScore(), SCORE.matchScore(), "Given match score " +
                "differ from actual on Matches page");
        Score matchScoreFromMatchPage = new BusinessLogicLayer(driver).getMatchScoreFromMatchPage(teamOne, teamTwo);
        Assert.assertEquals(matchScoreFromMatchPage.matchScore(), SCORE.matchScore(), "Given match score differ " +
                "from actual on CurrentMatch page");
    }
}
